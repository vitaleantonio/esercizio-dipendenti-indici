<style>

    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }

</style>
@if(count($employees)>0)

    <table>
        <tr>
            <th>Dipendente</th>
            <th>Indici - Peso - Voto</th>
            <th>Media</th>
        </tr>
        @foreach($employees as $employee)
            @php
                $sum = 0;
                $c = 0;
                $med = 0;
            @endphp

            <tr>
                <td>{{ $employee->name }} {{ $employee->surname }}</td>
                <td>
                    @foreach($employee->mark as $mark)
                        {{ $mark->name }} - {{ $mark->weight }} - {{ $mark->pivot->mark }}<br />
                    @endforeach
                </td>
                <td>
                    @foreach($employee->mark as $mark)
                        @php
                            $sum = $sum + ($mark->weight * $mark->pivot->mark);
                            $c++;
                        @endphp
                    @endforeach
                    @php
                        if($c != 0)
                        $med = $sum/$c ;
                        echo $med;
                    @endphp<br>
                </td>
            </tr>

        @endforeach
    </table>

@else
    <p> Nessun dato trovato, <a href="/">torna indietro</a> </p>
@endif
<a href="/">Home</a>