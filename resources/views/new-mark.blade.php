<form action="mark" method="post">
    {{csrf_field()}}
    <label>Seleziona un dipendente</label>
    <select name="dipendente">
        @if(count($employees)>0)

            @foreach($employees as $employee)

                <option value="{{$employee->id}}">{{ $employee->name }} {{ $employee->surname }}</option>

            @endforeach

        @else
            <option value="err">NO DATA</option>
        @endif

    </select>
    <br>
    <label>Seleziona un indice</label>
    <select name="indice">
        @if(count($indices)>0)

            @foreach($indices as $index)

                <option value="{{$index->id}}">{{ $index->name }}</option>

            @endforeach

        @else
            <option value="err">NO DATA</option>
        @endif

    </select>
    <br>
    <input type="number" placeholder="Inserisci voto" name="voto">

    <input type="submit">

</form>

<a href="/">Torna indietro</a>