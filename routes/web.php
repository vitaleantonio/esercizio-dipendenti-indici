<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/employee', function (){
    return view('new-employee');
});

Route::post('employee', 'EmployeeController@insert');

Route::get('/index', function(){
    return view('new-index');
});

Route::post('index', 'IndexController@insert');

Route::get('/mark', 'PageController@mark');

Route::post('mark', 'EmployeeController@mark');

Route::get('/show', 'PageController@show');
