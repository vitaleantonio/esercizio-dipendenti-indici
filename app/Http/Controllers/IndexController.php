<?php

namespace App\Http\Controllers;

use App\Index;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    public function insert(Request $req)
    {

        $newIndex = new Index();

        $name = $req->input('nome');
        $weight = $req->input('peso');

        $newIndex->name = $name;
        $newIndex->weight = $weight;

        $newIndex->save();

        return redirect('/');
    }

}
