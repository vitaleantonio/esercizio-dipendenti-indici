<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Index;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    public function mark()
    {

        $employees = Employee::all();
        $indices = Index::all();

        return view('new-mark')->with(compact('employees', 'indices'));

    }

    public function show(){

        $employees = Employee::all();
        $allMarks = DB::table('employee_index')->get();
        return view ('show')->with(compact('employees','allMarks'));

    }
}
