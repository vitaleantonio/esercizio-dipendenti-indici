<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Index;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    public function insert(Request $req)
    {

        $newEmployee = new Employee();

        $name = $req->input('nome');
        $surname = $req->input('cognome');

        $newEmployee->name = $name;
        $newEmployee->surname = $surname;

        $newEmployee->save();

        return redirect('/');

    }

    public function mark(Request $req)
    {

        $employeeId = $req->input('dipendente');
        $indexId = $req->input('indice');
        $mark = $req->input('voto');

        $employee = Employee::find($employeeId);
        $index = Index::find($indexId);


        if($employee->mark()->where('index_id', $indexId)->exists()){

            DB::table('employee_index')
                ->where('index_id', $indexId)
                ->update(['mark' => $mark]);

        }

        else {
            $employee->mark()->attach($index, ['mark' => $mark]);
        }

        return redirect('/');
    }
}
