<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

    public function mark()
    {

        return $this->belongsToMany(Index::class)->withPivot('mark');

    }

}
